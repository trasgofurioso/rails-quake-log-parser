class CreateMeans < ActiveRecord::Migration
  def change
    create_table :means do |t|
      t.string :code

      t.timestamps
    end
  end
end
