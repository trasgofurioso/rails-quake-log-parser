class RemoveMeanIdFromKill < ActiveRecord::Migration
  def up
    remove_column :kills, :mean_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
