require 'rails_helper'

describe GamesController, :type => :controller do
  describe "GET show" do
    render_views
    let(:game) { create :game }
    let(:p1) { create :player, game_id: game.id, name: 'Player 1' }
    let(:p2) { create :player, game_id: game.id, name: 'Player 2' }
    let!(:p1_kills) { create_list :kill, 2, game_id: game.id, killer_id: p1.id, mean: :MOD_SHOTGUN }
    let!(:p2_kills) { create_list :kill, 3, game_id: game.id, killer_id: p2.id }

    before(:each) { get :show, format: :json, id: game.id }

    it 'works' do
      expect(response).to have_http_status 200
    end
    
    it "returns the information for one game" do
      expect(json).to have_key(game.name)
      expect(json[game.name]).to have_key('total_kills')
      expect(json[game.name]).to have_key('players')
      expect(json[game.name]).to have_key('kills')
      expect(json[game.name]).to have_key('kills_by_mean')
      game.players.each do |player|
        expect(json[game.name]['players']).to include player.name
        expect(json[game.name]['kills'][player.name]).to eq player.kills.count
      end
      expect(json[game.name]['kills_by_mean']['MOD_SHOTGUN']).to be 2
    end
  end

  describe "GET index" do
    render_views
    let!(:games) { create_list :game, 3 }

    before(:each) { get :index, format: :json }    

    it 'works' do
      expect(response).to have_http_status 200
    end

    it 'returns a list of games' do
      expect(json.count).to be 3
    end
  end 
end
