# Rails Quake Log Parser

Import Quake game logs and get statistics. 

## Installation

Clone repo:

    $ git clone https://trasgofurioso@bitbucket.org/trasgofurioso/rails-quake-log-parser.git

Create gemset:

	$ rvm gemset create rails-quake-log-parser

**Note: Ruby 2.0 is required**

Install the project gems:

    $ bundle

Migrate database:

	$ rake db:migrate

## Usage

### CLI

Import a log file into the app:

    $ rake quake_log:import[/path/to/file]

See statistics for a game:

    $ rake quake_log:show[game_id]

See statistics for all games:

    $ rake quake_log:index

### API

**Note: The web-based API is read-only**

See statistics for game with id = 1:

    /games/1.json

See statistics for all games:

    /games.json