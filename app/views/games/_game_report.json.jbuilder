json.set!("game_#{game.id}") do |json|
  json.(game, :total_kills)
  json.players do
    json.array! game.players.collect { |player| player.name }
  end
  json.kills do
    game.players.each { |player| json.set! player.name, player.kills_count }
  end
  json.kills_by_mean do
    game.kills_by_mean.each do |kill|
      json.set! kill.mean, kill.count
    end
  end
end