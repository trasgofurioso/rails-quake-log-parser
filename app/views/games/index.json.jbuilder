json.(@games) do |game|
  json.partial! 'game_report', game: game
end
