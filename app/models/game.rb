class Game < ActiveRecord::Base
  include QuakeLogImporter
  include Renderable

  has_many :kills
  has_many :players

  def total_kills
    kills.count
  end

  def name
    "game_#{id}"
  end

  def kills_by_mean
  	kills.select('killer_id, mean, COUNT(*) as count').group('mean')
  end
end
