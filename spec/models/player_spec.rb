require 'rails_helper'

describe Player, :type => :model do
  let(:player) { create :player }

  it 'counts kills' do
    create_list :kill, 2, killer_id: player.id
    expect(player.kills_count).to be 2
  end

  it 'subtracts one kill if killed by the world' do
  	create_list :kill, 2, killer_id: player.id
  	create :kill, victim_id: player.id
  	expect(player.kills_count).to be 1
  end

  it 'counts deaths' do
    create_list :kill, 3, victim_id: player.id
    expect(player.deaths.count).to be 3
  end  
end
