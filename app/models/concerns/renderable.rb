require 'render_anywhere'

module Renderable
  extend ActiveSupport::Concern
  include RenderAnywhere

  included do
  end

  module ClassMethods
  end
end