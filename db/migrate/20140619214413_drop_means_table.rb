class DropMeansTable < ActiveRecord::Migration
  def up
    drop_table :means
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end