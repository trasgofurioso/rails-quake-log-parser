class AddVictimIdToKill < ActiveRecord::Migration
  def change
    add_column :kills, :victim_id, :integer
  end
end
