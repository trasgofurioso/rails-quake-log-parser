require 'spec_helper'

shared_examples_for Renderable do
  it 'renders a view' do
    expect(described_class.new).to respond_to :render
  end
end