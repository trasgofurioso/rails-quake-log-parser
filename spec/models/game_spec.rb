require 'rails_helper'
require 'models/concerns/quake_log_importer_shared_examples.rb'
require 'models/concerns/renderable_shared_examples.rb'

describe Game, :type => :model do
  it_behaves_like QuakeLogImporter
  it_behaves_like Renderable

  let(:game) { create :game }

  it 'counts total kills' do
    create_list :kill, 2, game_id: game.id
    expect(game.total_kills).to eq 2
  end

  it 'lists the players' do
    create_list :player, 2, game_id: game.id
    players = Player.where(game_id: game.id)
    expect(players).to be_present
    players.each do |player|
      expect(game.players).to include player
    end
  end

  it 'has a name' do
    expect(game.name).to eq "game_#{game.id}"
  end

  it 'lists kills by mean' do
    create_list :kill, 2, game_id: game.id, mean: :MOD_SHOTGUN
    expect(game.kills_by_mean.first.mean).to eq "MOD_SHOTGUN"
    expect(game.kills_by_mean.first.count).to be 2
  end
end
