include Term::ANSIColor
require 'pp'

namespace :quake_log do
  desc "Imports a game log"
  task :import, [:file_path] => [:environment] do |t, args|
    begin
      file = IO.read(args.file_path)
    rescue TypeError
      print red, "I need a log file to do this!\n"
      print "Usage: rake quake_log:import[/var/log/quake.log]\n", reset
      abort
    rescue Errno::ENOENT
      print red, "Can't open the file! #{args.file_path}\n"
      print "Usage: rake quake_log:import[/var/log/quake.log]\n", reset
      abort
    end
    begin
      games = Game.quake_log_import(file)  
    rescue Exception => e
      print red, "An error ocurred!\n", reset
      raise e
    else
      print green, "Quake log imported!\n"
      print "#{games.count} games where created\n"
      print "To see the details run the following commands\n", reset
      games.each do |game|
        print yellow, "rake quake_log:show[#{game.id}]\n", reset
      end
    end
  end

  desc "Shows a game report"
  task :show, [:game_id] => [:environment] do |t, args|
    begin
      game = Game.find(args.game_id)
    rescue ActiveRecord::RecordNotFound
      print red, "This game does not exist! #{args.game_id}\n"
      print "Usage: rake quake_log:show[1]\n", reset
      abort
    else
      json = game.render partial: 'games/game_report.json.jbuilder', locals: { game: game }
      print JSON.pretty_generate(JSON.parse(json)), "\n"
    end
  end

  desc "Shows all game reports"
  task :index => [:environment] do |t|
    Game.all.each do |game|
      json = game.render partial: 'games/game_report.json.jbuilder', locals: { game: game }
      print JSON.pretty_generate(JSON.parse(json)), "\n"
    end
  end
end
