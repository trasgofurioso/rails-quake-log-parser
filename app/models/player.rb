class Player < ActiveRecord::Base
  belongs_to :game

  has_many :kills, foreign_key: :killer_id
  has_many :deaths, class_name: Kill, foreign_key: :victim_id

  def kills_count
  	kills.count - deaths.where(killer_id: nil).count
  end
end
