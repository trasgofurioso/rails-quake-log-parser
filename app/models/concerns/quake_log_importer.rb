module QuakeLogImporter
  extend ActiveSupport::Concern

  included do
  end

  module ClassMethods
    def quake_log_import(log)
      games = []
      game = Game.new
      log.split(/\n/).each do |line|
        if line.include?('InitGame:')
          game = Game.create(params_for_game(line)) 
          games.append(game)
        end
        if line.include?('ClientUserinfoChanged:')
          Player.find_or_create_by(params_for_player(line, game))
        end
        if line.include?('Kill:')
          Kill.create(params_for_kill(line, game)) 
        end
      end
      games
    end

    private
    def params_for_game(line)
      params = line.split(': ')[1].split
      { params: params[0].strip }
    end

    def params_for_player(line, game)
      params = line.split(': ')[1].split
      { 
        game_id: game.id,
        quake_id: params[0].strip,
        name: params[1].split(/\\/)[1],
      }
    end

    def params_for_kill(line, game)
      params = line.split(': ')[1].split
      # If killer is not present it is the <world>
      killer = Player.where(quake_id: params[0], game_id: game.id).first
      killer_id = killer.id if killer.present?
      victim = Player.where(quake_id: params[1], game_id: game.id).first
      victim_id = victim.id if victim.present?
      { 
        game_id: game.id,
        killer_id: killer_id,
        victim_id: victim_id,
        mean: line.split(': ')[2].split.last
      }
    end
  end
end