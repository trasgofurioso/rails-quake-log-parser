require 'spec_helper'

shared_examples_for QuakeLogImporter do
  describe 'QuakeLogImporter' do
    let(:log) { IO.read('spec/factories/quake.log') }
    let(:games) { described_class.quake_log_import(log) }
    let(:players) { games.last.players }
    let(:kills) { games.last.kills }

    before { games }

    it 'creates games and returns them in an array' do
      expect(games).to be_a Array
      expect(games.count).to be 2
    end

    it 'creates players' do
      expect(players).to be_present
    end

    it 'does not create a Player twice' do
      expect(players.where(quake_id: 2).count).to be 1
    end

    it 'updates a Player' do
      expect(players.where(quake_id: 2, name: 'Isgalamido')).to be_present
    end

    it 'creates a Kill by the world' do
      kill = kills.where(killer_id: nil, victim_id: 1).first
      expect(kill).to be_present
      expect(kill).to be_MOD_TRIGGER_HURT
    end

    it 'creates a Kill by a player' do
      kill = kills.where(killer_id: 1, victim_id: 2).first
      expect(kill).to be_present
      expect(kill).to be_MOD_ROCKET_SPLASH
    end
  end
end